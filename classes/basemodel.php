<?php

abstract class BaseModel
{
    protected $database, $config;
    public $view;

    public function __construct($config)
    {
        $this->config = $config;
        $this->database = new PDO("mysql:host=".$config['db_location'].";dbname=".$config['db_name'], $config['db_user'], $config['db_pass']);

        $this->view = new stdClass();
        $this->view->modelErrors = array();
        $this->view->hasError = false;
    }

    public function addModelError($field, $modelError)
    {
        $this->view->modelErrors[$field] = $modelError;
    }

    public function hasError()
    {
        return count($this->view->modelErrors) > 0 ? true : false;
    }


    public function add($property, $value)
    {
        $this->view->$property = $value;
    }

    //META DATA
    public function setPageTitle($x)
    {
        $this->view->pageTitle = $this->config['name'].' | '.$x;
    }

    //MESSAGES
    public function setMesssage($type, $title, $message)
    {
        if($type == MessageType::Success)
        {
            $this->setSuccessMessage($title, $message);
        }
        else if($type == MessageType::Error)
        {
            $this->setErrorMessage($title, $message);
        }
        else if($type == MessageType::Warning)
        {
            $this->setWarningMessage($title, $message);
        }
        else
        {
            $this->setInfoMessage($title, $message);
        }
    }

    public function setSuccessMessage($title, $message)
    {
        $this->view->message =  "<div class=\"alert alert-success\"><span class=\"title\">".$title."</span><span class=\"message\">".$message."</span><span class=\"close\">(click to close)</span></div>";
    }

    public function setErrorMessage($title, $message)
    {
        $this->view->message =  "<div class=\"alert alert-danger\"><span class=\"title\">".$title."</span><span class=\"message\">".$message." (click to close)</span><span class=\"close\">(click to close)</span></div>";
    }

    public function setWarningMessage($title, $message)
    {
        $this->view->message =  "<div class=\"alert alert-warning\"><span class=\"title\">".$title."</span><span class=\"message\">".$message." (click to close)</span><span class=\"close\">(click to close)</span></div>";
    }

    public function setInfoMessage($title, $message)
    {
        $this->view->message =  "<div class=\"alert alert-info\"><span class=\"title\">".$title."</span><span class=\"message\">".$message." (click to close)</span><span class=\"close\">(click to close)</span></div>";
    }
}