<?php

class Home extends BaseController
{
    public function __construct($action, $urlParams)
    {
        parent::__construct($action, $urlParams);

        session_start();

        if (!isset($_SESSION['Username']) && !isset($_SESSION['Username']) && $_SESSION['LoggedIn'] != 1)
        {
            $this->Redirect('home');
        }
    }

    public function Index()
    {
        $this->Redirect('app', 'invest');
    }
}
