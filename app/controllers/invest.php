<?php

class Invest extends BaseController
{
    public function __construct($action, $urlParams)
    {
        parent::__construct($action, $urlParams);

        session_start();

        if (!isset($_SESSION['Username']) && !isset($_SESSION['Username']) && $_SESSION['LoggedIn'] != 1)
        {
            $this->Redirect('home');
        }
    }

    protected function Index()
    {
        $model = new InvestModel("Index", false);

        $model->setPageTitle("Invest");
        $this->ReturnView($model->view);
    }

    protected function Detail()
    {
        $model = new InvestModel("Detail");

        $model->setPageTitle("Detail");
        $this->ReturnView($model->view);
    }
}