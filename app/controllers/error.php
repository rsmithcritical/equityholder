<?php

class Error extends BaseController
{
    protected function BadUrl()
    {
        $model = new ErrorModel("BadUrl");
        $model->add("errormessage", "This url does not exist!");

        $model->setPageTitle("Error - No such page");
        $this->ReturnView($model);
    }
}