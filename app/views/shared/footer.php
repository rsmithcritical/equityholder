        <footer>
            <div class="footer-container">
                <div class="container">
                    <p class="footer-copy">
                        <span>&copy; EquityHolder.com 2015</span>
                        <span class="pull-right footer-sub-nav">
                            <a href="#">T&Cs</a> |
                            <a href="#">Privacy Policy</a> |
                            <a href="#">Cookies</a>
                        </span>
                    </p>
                </div>
            </div>
        </footer>
    </body>
</html>