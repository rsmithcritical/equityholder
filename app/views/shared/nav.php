<nav class="navbar navbar-default main-nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/app">
                <img src="/app/public/img/logo.png" alt="" />
            </a>
        </div>
        <div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav navbar-right main-account-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $model->account->full_name; ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li style="margin-left: 20px;">Balance: <strong>£234,354</strong></li>
                        <li class="divider"></li>
                        <li><a href="#">Profile</a></li>
                        <li><a href="#">Messages</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="/app/account/logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
            <ul class="nav navbar-nav navbar-right main-site-nav">
                <li><a href="#" class="selected">INVEST</a></li>
                <li><a href="#">PORTFOLIO</a></li>
                <li><a href="#">LANDLORD</a></li>
                <li><a href="#">SUPPORT</a></li>
            </ul>
        </div>
    </div>
</nav>
<nav class="navbar navbar-default sub-nav">
    <div class="container">

        <div class="collapse navbar-collapse" id="sub-nav">
            <ul class="nav navbar-nav navbar-left main-site-sub-nav">
                <li><a href="/app/invest/" class="selected">FIND EQUITY</a></li>
                <li><a href="#">IN PROGRESS</a></li>
                <li><a href="#">HISTORY</a></li>
            </ul>
        </div>
    </div>
</nav>