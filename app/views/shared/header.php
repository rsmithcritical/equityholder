<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title><?= $model->pageTitle; ?></title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="EquityHolder.com">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">


<link rel="icon" href="#">
<link rel="apple-touch-icon" href="#">
<link rel="apple-touch-icon" sizes="72x72" href="#">
<link rel="apple-touch-icon" sizes="114x114" href="#">

<link href='http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,900' rel='stylesheet' type='text/css'>
<link href="/app/public/css/bootstrap.css" rel='stylesheet' type='text/css'>
<link href="/app/public/css/bootstrap-theme.css" rel='stylesheet' type='text/css'>
<link href="/app/public/css/kendo.common.min.css" rel='stylesheet' type='text/css'>
<link href="/app/public/css/kendo.bootstrap.min.css" rel='stylesheet' type='text/css'>
<link href="/app/public/css/style.css" rel='stylesheet' type='text/css'>

<script type="text/javascript" src="/app/public/js/jquery_1.11.1.min.js"></script>
<script type="text/javascript" src="/app/public/js/bootstrap.js"></script>
<script type="text/javascript" src="/app/public/js/kendo.all.min.js"></script>



