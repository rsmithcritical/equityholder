<div class="container" id="main-content">
    <div class="row">
        <div class="col-md-3">
            <div class="filter-container">
                <h2>Find Equity</h2>
                <label>Search</label>
                <input type="text" placeholder="search..." class="search">
                <br><br>

                <label>Min Value: £<span class="min-value-display">500,000</span></label>

                <div class="clearfix"></div>
                <div class="min-value-slider" style="width: 100%"></div>
                <br><br>

                <label>Max Value: £<span class="min-value-display">100,000,000</span></label>

                <div class="clearfix"></div>
                <div class="max-value-slider" style="width: 100%"></div>
                <br><br>
            </div>
        </div>
        <div class="col-md-9">
            <div id="property-list"></div>
            <div id="pager" class="k-pager-wrap"></div>
        </div>
    </div>
</div>

<script src="/app/public/js/active-properties.js"></script>
<script>
    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    function toInt(x) {
        return x.toFixed(0);
    }

    //temp

</script>
<script type="text/x-kendo-template" id="property-template">
    <div class="property-list-item">
        <h3><a href="/app/invest/detail">#:name_number#, #:address_line_1#, #:city#, #:postcode#</a> <span class="pull-right">£#:kendo.toString(numberWithCommas(target), "c")#</span></h3>

        <div class="row">
            <div class="col-md-3">
                <img src="/images/thumb/#= id #.jpg" alt=""/>

                <div class="clearfix"></div>
                <p style="margin-top: 10px;">
                    <span class="glyphicon glyphicon-user" title="investors"></span> #: investors #
                </p>
            </div>
            <div class="col-md-8">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: #:toInt((funded / target) * 100)#%;"></div>
                </div>
                <p>
                    <span>£#:numberWithCommas(funded)# funded</span>
                    <span class="pull-right">#:toInt((funded / target) * 100)#%</span>
                </p>

                <p>#:description# ...</p>
                <a href="/app/invest/detail" class="btn btn-default btn-sm pull-left"
                   style="margin-right: 10px;">more details</a>
                <a href="##" class="btn btn-primary btn-sm invest-quick-launch pull-left" style="margin-right: 10px;">invest</a>

                <div class="pull-left" style="display: none">
                    <span class="description pull-left" style="font-size: 16px; line-height: 28px; margin-right: 10px;">Amount:</span>
                    <input class="currency-picker pull-left" type="number" value="500" min="500" max="1000000"/>
                    <a href="##" class="btn btn-default btn-sm pull-left" style="margin-left: 10px;">buy me in!</a>
                </div>
                <div class="clearfix"></div>
                <p class="pull-right" style="margin-top:15px; font-size: 10px;">Added on 29/01/2015 by Ryan Smith,
                    EquityHolder.</p>
            </div>
        </div>

    </div>
</script>

<script>
    $(function () {
        var dataSource = new kendo.data.DataSource({
            data: active_properties,//get opportunities from db,
            pageSize: 5,
            change: function () {
                setTimeout(function () {
                    $('.invest-quick-launch').click(function (e) {
                        e.preventDefault();
                        $(this).next().show();
                        $(this).hide();
                        $(this).next().children(".currency-picker").kendoNumericTextBox({
                            format: "£#",
                            decimals: 3,
                            step: 100
                        })
                    });
                }, 1000);
            }
        });

        $("#pager").kendoPager({
            dataSource: dataSource
        });

        var propertyList = $("#property-list").kendoListView({
            dataSource: dataSource,
            template: kendo.template($("#property-template").html())
        });

        $('.search').keyup(function () {
            var term = $(this).val();
            dataSource.filter({field: "city", operator: "contains", value: term});
        });

        var minValueSlider = $(".min-value-slider").kendoSlider({
            increaseButtonTitle: "Right",
            decreaseButtonTitle: "Left",
            min: 500000,
            max: 100000000,
            smallStep: 500000,
            largeStep: 1000000,
            slide: minValueSliderOnSlide
        }).data("kendoSlider");

        function minValueSliderOnSlide(e) {
            $('.min-value-display').html(numberWithCommas(e.value));
            dataSource.filter({field: "target", operator: "greaterthan", value: e.value});
        }

        var maxValueSlider = $(".max-value-slider").kendoSlider({
            increaseButtonTitle: "Right",
            decreaseButtonTitle: "Left",
            min: 500000,
            max: 100000000,
            value: 100000000,
            smallStep: 500000,
            largeStep: 1000000,
            slide: maxValueSliderOnSlide
        }).data("kendoSlider");

        function maxValueSliderOnSlide(e) {
            $('.max-value-display').html(numberWithCommas(e.value));
            dataSource.filter({field: "target", operator: "lessthan", value: e.value});
        }


    });
</script>
