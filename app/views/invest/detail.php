<script type="text/javascript" src="/app/public/js/jssor.js"></script>
<script type="text/javascript" src="/app/public/js/jssor.slider.js"></script>
<script>

    jQuery(document).ready(function ($) {
        var options = {
            $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
            $AutoPlayInterval: 1500,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
            $PauseOnHover: 1,                                //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

            $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
            $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
            $SlideDuration: 800,                                //Specifies default duration (swipe) for slide in milliseconds

            $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                $Transitions: [{
                    $Duration: 1200,
                    x: -0.3,
                    $During: {$Left: [0.3, 0.7]},
                    $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear},
                    $Opacity: 2
                }],            //[Required] An array of slideshow transitions to play slideshow
                $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
            },

            $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                $ChanceToShow: 1                               //[Required] 0 Never, 1 Mouse Over, 2 Always
            },

            $ThumbnailNavigatorOptions: {                       //[Optional] Options to specify and enable thumbnail navigator or not
                $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                $SpacingX: 8,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                $DisplayPieces: 10,                             //[Optional] Number of pieces to display, default value is 1
                $ParkingPosition: 360                          //[Optional] The offset position to park thumbnail
            }
        };

        var jssor_slider1 = new $JssorSlider$("slider1_container", options);
        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizes
        function ScaleSlider() {
            var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
            if (parentWidth)
                jssor_slider1.$ScaleWidth(Math.max(Math.min(parentWidth, 800), 300));
            else
                window.setTimeout(ScaleSlider, 30);
        }

        ScaleSlider();

        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        //responsive code end
    });
</script>
<div class="container" id="main-content">
    <div class="row">
        <div class="col-md-9">
            <h2 style="margin-top: 0">10 Lyndhurst Road, London, EC2</h2>

            <div class="clearfix"></div>

            <!-- Jssor Slider Begin -->
            <!-- You can move inline styles to css file or css block. -->
            <div id="slider1_container"
                 style="position: relative; top: 0px; left: 0px; width: 800px; height: 456px;  overflow: hidden;">

                <!-- Loading Screen -->
                <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                    <div
                        style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;  top: 0px; left: 0px;width: 100%;height:100%;">
                    </div>
                    <div
                        style="position: absolute; display: block; background: url(/app/public/img/loading.gif) no-repeat center center; top: 0px; left: 0px;width: 100%;height:100%;">
                    </div>
                </div>

                <!-- Slides Container -->
                <div u="slides"
                     style="cursor: move; position: absolute; left: 0px; top: 0px; width: 800px; height: 356px; overflow: hidden;">
                    <div>
                        <img u="image" src="/images/1/1.jpg"/>
                        <img u="thumb" src="/images/1/1.jpg"/>
                    </div>
                    <div>
                        <img u="image" src="/images/1/2.jpg"/>
                        <img u="thumb" src="/images/1/2.jpg"/>
                    </div>
                    <div>
                        <img u="image" src="/images/1/3.jpg"/>
                        <img u="thumb" src="/images/1/3.jpg"/>
                    </div>
                    <div>
                        <img u="image" src="/images/1/4.jpg"/>
                        <img u="thumb" src="/images/1/4.jpg"/>
                    </div>
                    <div>
                        <img u="image" src="/images/1/5.jpg"/>
                        <img u="thumb" src="/images/1/5.jpg"/>
                    </div>
                    <div>
                        <img u="image" src="/images/1/6.jpg"/>
                        <img u="thumb" src="/images/1/6.jpg"/>
                    </div>
                    <div>
                        <img u="image" src="/images/1/7.jpg"/>
                        <img u="thumb" src="/images/1/7.jpg"/>
                    </div>
                    <div>
                        <img u="image" src="/images/1/8.jpg"/>
                        <img u="thumb" src="/images/1/8.jpg"/>
                    </div>
                    <div>
                        <img u="image" src="/images/1/9.jpg"/>
                        <img u="thumb" src="/images/1/9.jpg"/>
                    </div>
                    <div>
                        <img u="image" src="/images/1/10.jpg"/>
                        <img u="thumb" src="/images/1/10.jpg"/>
                    </div>
                </div>

                <!-- Arrow Navigator Skin Begin -->
                <style>
                    /* jssor slider arrow navigator skin 05 css */
                    /*
                    .jssora05l              (normal)
                    .jssora05r              (normal)
                    .jssora05l:hover        (normal mouseover)
                    .jssora05r:hover        (normal mouseover)
                    .jssora05ldn            (mousedown)
                    .jssora05rdn            (mousedown)
                    */
                    .jssora05l, .jssora05r, .jssora05ldn, .jssora05rdn {
                        position: absolute;
                        cursor: pointer;
                        display: block;
                        background: url(/app/public/img/a17.png) no-repeat;
                        overflow: hidden;
                    }

                    .jssora05l {
                        background-position: -10px -40px;
                    }

                    .jssora05r {
                        background-position: -70px -40px;
                    }

                    .jssora05l:hover {
                        background-position: -130px -40px;
                    }

                    .jssora05r:hover {
                        background-position: -190px -40px;
                    }

                    .jssora05ldn {
                        background-position: -250px -40px;
                    }

                    .jssora05rdn {
                        background-position: -310px -40px;
                    }
                </style>
                <!-- Arrow Left -->
        <span u="arrowleft" class="jssora05l" style="width: 40px; height: 40px; top: 158px; left: 8px;">
        </span>
                <!-- Arrow Right -->
        <span u="arrowright" class="jssora05r" style="width: 40px; height: 40px; top: 158px; right: 8px">
        </span>
                <!-- Arrow Navigator Skin End -->

                <!-- Thumbnail Navigator Skin Begin -->
                <div u="thumbnavigator" class="jssort01"
                     style="position: absolute; width: 800px; height: 100px; left:0px; bottom: 0px;">
                    <!-- Thumbnail Item Skin Begin -->
                    <style>
                        /* jssor slider thumbnail navigator skin 01 css */
                        /*
                        .jssort01 .p           (normal)
                        .jssort01 .p:hover     (normal mouseover)
                        .jssort01 .pav           (active)
                        .jssort01 .pav:hover     (active mouseover)
                        .jssort01 .pdn           (mousedown)
                        */
                        .jssort01 .w {
                            position: absolute;
                            top: 0px;
                            left: 0px;
                            width: 100%;
                            height: 100%;
                        }

                        .jssort01 .c {
                            position: absolute;
                            top: 0px;
                            left: 0px;
                            width: 68px;
                            height: 68px;
                            border: #000 2px solid;
                        }

                        .jssort01 .p:hover .c, .jssort01 .pav:hover .c, .jssort01 .pav .c {
                            background: url(/app/public/img/t01.png) center center;
                            border-width: 0px;
                            top: 2px;
                            left: 2px;
                            width: 68px;
                            height: 68px;
                        }

                        .jssort01 .p:hover .c, .jssort01 .pav:hover .c {
                            top: 0px;
                            left: 0px;
                            width: 70px;
                            height: 70px;
                            border: #fff 1px solid;
                        }

                        div.c {
                            border: none !important;
                        }

                    </style>
                    <div u="slides" style="cursor: move;">
                        <div u="prototype" class="p"
                             style="position: absolute; width: 72px; height: 72px; top: 0; left: 0;">
                            <div class=w>
                                <div u="thumbnailtemplate"
                                     style=" width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;"></div>
                            </div>
                            <div class=c>
                            </div>
                        </div>
                    </div>
                    <!-- Thumbnail Item Skin End -->
                </div>
            </div>

            <h2>Full description</h2>

            <p>
                This truly exceptional property has been renovated to the highest possible standard and provides grand
                period living close to Hampstead Village (400m) with a large garden & indoor swimming pool as well as a
                Garage and Carriage Drive behind gates
            </p>

            <p>
                The house also benefits from a spectacular double volume entrance hall, panelled study, grand double
                reception room and two large dining rooms. Additionally there is a cinema room, a particularly beautiful
                indoor swimming pool, garaging for 3 to 4 cars, and off street parking for another 3 to 4 cars.
                The master bedroom suite benefits from far reaching views across London as well as his and hers dressing
                rooms and bathrooms. There are 6 further principal bedrooms, 5 further bathrooms (all en-suite).
                Additionally there is a separate staff flat to the lower ground floor. The garden is large (over 100'
                wide and deep) level and mostly laid to lawn with an outdoor swimming pool and summer house.
            </p>

            <p>
                The house is within 400m of the shops and transport facilities of Hampstead Village. Belsize Park is
                equally close.
                Marble Arch is only 3.3miles with a an average journey time of 18 minutes (source: Google maps)
            </p>
        </div>
        <div class="col-md-3">
            <h2 style="margin-top: 0">Progress</h2>

            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 65%;"></div>
            </div>
            <span>65%</span>
            <span class="pull-right">£30,434,405 / £46,500,000</span>

            <br/><br/>

            <h2 style="margin-top: 0">Invest</h2>

            <div>
                <span class="description" style="font-size: 16px; line-height: 28px;">Amount:</span>
                <input id="currency" type="number" value="500" min="500" max="1000000" class="pull-right"/>
                <br/>
                <a href="#" class="btn btn-default btn-lg" style="width: 100%; margin-top: 10px;">buy me in!</a>

            </div>
        </div>
    </div>
</div>
<br><br><br>

<script>
    $(document).ready(function () {

        $("#currency").kendoNumericTextBox({
            format: "£#",
            decimals: 3,
            step: 100
        })

    });
</script>
