<?php

return array(
    'name' => 'EquityHolder',
    'environment' => $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? 'local' : 'test',
    'db_location' => 'localhost',
    'db_name' => 'equityholder_db',
    'db_user' => 'root',
    'db_pass' => ''
);