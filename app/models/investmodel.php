<?php

class InvestModel extends CommonModel
{
    public function __construct($action, $isPost = false, $params = array())
    {
        parent::__construct($action);

        if($isPost)
        {
            $this->params = call_user_func_array(array($this, $action.'_POST'), $params);
        }
        else
        {
            $this->params = call_user_func_array(array($this, $action), $params);
        }

        $this->GetAccountInfo();
    }

    public function Index()
    {


    }

    public function Detail()
    {

    }

}