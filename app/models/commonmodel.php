<?php

class CommonModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct(include('conf/app.conf.php'));
    }

    public function GetAccountInfo()
    {
        $e = $_SESSION['Username'];

        $sql = "SELECT * FROM users WHERE email=:email";
        if($stmt = $this->database->prepare($sql))
        {
            $stmt->bindParam(':email', $e, PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch();

            $stmt->closeCursor();

            $this->view->account = new stdClass();
            $this->view->account->first_name = $row['id'];
            $this->view->account->first_name = $row['first_name'];
            $this->view->account->last_name = $row['last_name'];
            $this->view->account->full_name = $row['first_name'].' '.$row['last_name'];
        }
    }

}