<?php

//Require base classes
require_once '../classes/loader.php';
require_once '../classes/basecontroller.php';
require_once '../classes/basemodel.php';

//Require helpers
require_once 'helpers/htmlhelper.php';

//Require models
require_once 'models/commonmodel.php';
require_once 'models/errormodel.php';
require_once 'models/investmodel.php';

//Require controllers
require_once 'controllers/error.php';
require_once 'controllers/home.php';
require_once 'controllers/invest.php';
require_once 'controllers/account.php';

//create controllers and execute the action
$loader = new Loader($_GET);

$controller = $loader->CreateController();
$controller->ExecuteAction();