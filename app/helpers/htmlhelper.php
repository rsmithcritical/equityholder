<?php

class HTMLHelper
{
    public function __construct()
    {

    }

    public function DisplayErrorFor($m, $field)
    {
        if(isset($m) && isset($m->modelErrors[$field]))
        {
            echo '<p class="alert alert-danger" style="padding: 5px 5px; margin: 5px 0;">'.$m->modelErrors[$field].'</p>';
        }
    }
}