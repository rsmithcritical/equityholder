<?php

class Home extends BaseController
{
    public function __construct($action, $urlParams)
    {
        parent::__construct($action, $urlParams);

        session_start();

        if (isset($_SESSION['Username']) && isset($_SESSION['LoggedIn']) && $_SESSION['LoggedIn'] == 1)
        {
            $this->Redirect('app', 'invest');
        }
    }

    protected function Index()
    {
        $model = new HomeModel("Index");

        $model->setPageTitle('Home');
        $this->ReturnView($model->view);
    }

}