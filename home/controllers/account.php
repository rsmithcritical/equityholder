<?php

class Account extends BaseController
{

    public function __construct($action, $urlParams)
    {
        parent::__construct($action, $urlParams);

        session_start();

        if (isset($_SESSION['Username']) && isset($_SESSION['LoggedIn']) && $_SESSION['LoggedIn'] == 1)
        {
            $this->Redirect('app', 'invest');
        }
    }

    /* SIGN UP */
    protected function SignUp()
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            //POST
            $model = new AccountModel("SignUp", true);

            //Error checking
            if($model->hasError())
            {
                $model->setPageTitle('Sign Up');
                $this->ReturnViewByName("SignUp", $model->view);
                exit();
            }

            $model->setMesssage(MessageType::Success, 'Sign-up Success!', 'Welcome to EquityHolder! Please check your email and follow the instructions to complete the registration process.');
            $model->setPageTitle('Sign Up');

            $this->ReturnViewByName("checkyouremail", $model->view, 'layout_no_footer');
        }
        else
        {
            //GET
            $model = new AccountModel("SignUp");

            $model->setPageTitle('Sign Up');
            $this->ReturnViewByName("signup", $model->view, 'layout_no_footer');
        }
    }

    /* LOG IN */
    protected function Login()
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            //POST
            $model = new AccountModel("Login", true);

            //Error checking
            if($model->hasError())
            {
                $model->setPageTitle('Login');
                $this->ReturnViewByName("Login", $model->view, 'layout_no_footer');
                exit();
            }

            $this->Redirect('app', 'invest');
        }
        else
        {
            //GET
            $model = new AccountModel("Login");

            $model->setPageTitle('Login');
            $this->ReturnViewByName("Login", $model->view, 'layout_no_footer');
        }
    }

    /* PASSWORD RESET */
    protected function Password()
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            //POST
            $model = new AccountModel("Password", true);

            //Error checking
            if($model->hasError())
            {
                //Model has errors return values for display
                $model->view->email = isset($_POST['email']) ? $_POST['email'] : null;
                $model->view->postcode = isset($_POST['postcode']) ? $_POST['postcode'] : null;

                $model->setPageTitle('Forgot Password');
                $this->ReturnViewByName("Password", $model->view, 'layout_no_footer');
                exit();
            }

            $model->setMesssage(MessageType::Success, 'Password Reset Successful!', 'Your password has been reset, please check your inbox!');
            $model->setPageTitle('Password Reset');

            $this->ReturnViewByName("passwordreset", $model->view, 'layout_no_footer');
        }
        else
        {
            //GET
            $model = new AccountModel("Password");

            $model->setPageTitle('Reset Password');
            $this->ReturnViewByName('password', $model->view, 'layout_no_footer');
        }
    }

    /* VERIFY */
    protected function Verify($v, $e)
    {
        $model = new AccountModel("Verify", false, $this->urlParams);

        $model->setPageTitle("Account Verified");
        $this->ReturnView($model->view, 'layout_registration');
    }

    /* RESET PASSWORD */
    protected function ResetPassword($v = '', $e = '')
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            //POST
            $model = new AccountModel("ResetPassword", true);

            //Error checking
            if($model->hasError())
            {
                //Model has errors, add params to model to repopulate form
                $model->view->id = $_POST['id'];
                $model->view->id = $_POST['email'];

                $model->setPageTitle("Password Reset");
                $this->ReturnViewByName('resetpassword', $model->view, 'layout_registration');
                exit();
            }

            $model->setSuccessMessage('Password Reset', 'Your password had been reset, please sign in.');
            $this->ReturnViewByName('Login', $model->view);
        }
        else
        {
            //GET
            $model = new AccountModel("ResetPassword", false, $this->urlParams);

            $model->setPageTitle("Reset Password");
            $this->ReturnView($model->view, 'layout_registration');
        }
    }

    /* COMPLETE SIGN UP */
    protected function Complete()
    {
        $params = array
        (
            'id'    =>  $_POST['id'],
            'email' =>  $_POST['email'],
            'first_name'  =>  $_POST['first_name'],
            'last_name'  =>  $_POST['last_name'],
            'password'  =>  $_POST['password'],
            'confirm_password' => $_POST['confirm_password'],
            'house_name_number'  =>  $_POST['house_name_number'],
            'address_line_1'  =>  $_POST['address_line_1'],
            'address_line_2'  =>  $_POST['address_line_2'],
            'city'  =>  $_POST['city'],
            'postcode'  =>  $_POST['postcode'],
            'account_type' => $_POST['account_type']
        );

        $model = new AccountModel("Complete", true, $params);

        //Error checking
        if($model->hasError())
        {
            //Model has errors, add params to model to repopulate form
            $model->view->id = $params['id'];
            $model->view->email = isset($params['email']) ? $params['email'] : null;
            $model->view->first_name = isset($params['first_name']) ? $params['first_name'] : null;
            $model->view->last_name = isset($params['last_name']) ? $params['last_name'] : null;
            $model->view->house_name_number = isset($params['house_name_number']) ? $params['house_name_number'] : null;
            $model->view->address_line_1 = isset($params['address_line_1']) ? $params['address_line_1'] : null;
            $model->view->address_line_2 = isset($params['address_line_2']) ? $params['address_line_2'] : null;
            $model->view->city = isset($params['city']) ? $params['city'] : null;
            $model->view->postcode = isset($params['postcode']) ? $params['postcode'] : null;
            $model->view->account_type = isset($params['account_type']) ? $params['account_type'] : null;

            $model->setPageTitle("Account Verified");
            $this->ReturnViewByName('verify', $model->view, 'layout_registration');
            exit();
        }

        $model->setPageTitle("Complete Registration");
        $model->setMesssage(MessageType::Success, 'Account Set Up Complete', 'Signed in as, '.$_POST['email'].'!');

        //Login
        session_start();
        $_SESSION['Username'] = $_POST['email'];
        $_SESSION['LoggedIn'] = 1;

        //ACCOUNT COMPLETION: Redirect to /APP/Invest/Index
        $this->Redirect('app', 'invest', 'index');
    }
}
