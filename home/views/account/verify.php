<div class="login-container">
    <img src="/home/public/img/logo.png" alt="" />
    <form class="login-form" action="/home/account/complete" method="post">
        <h2>
            Complete Sign Up
            <span class="pull-right" style="line-height: 36px; font-size: 14px;"><span class="current-wizard"></span>/3</span>
        </h2>

        <div class="wizard-page" data-target="1">
            <div class="form-group">
                <label>First Name</label>
                <input type="text" name="first_name" class="form-control" placeholder="Enter your first name" <?php $this->htmlHelper->DisplayValueFor($model, 'first_name'); ?> />
                <?php $this->htmlHelper->DisplayErrorFor($model, 'first_name'); ?>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" name="last_name" class="form-control" placeholder="Enter your last name" <?php $this->htmlHelper->DisplayValueFor($model, 'last_name'); ?> />
                <?php $this->htmlHelper->DisplayErrorFor($model, 'last_name'); ?>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" placeholder="Enter your password" />
                <?php $this->htmlHelper->DisplayErrorFor($model, 'password'); ?>

            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" placeholder="Confirm your password">
                <?php $this->htmlHelper->DisplayErrorFor($model, 'confirm_password'); ?>
            </div>
        </div>

        <div  class="wizard-page" data-target="2">
            <div class="form-group">
                <label>House name/number</label>
                <input type="text" name="house_name_number" class="form-control" placeholder="Enter your house name or number" <?php $this->htmlHelper->DisplayValueFor($model, 'house_name_number'); ?>>
                <?php $this->htmlHelper->DisplayErrorFor($model, 'house_name_number'); ?>
            </div>
            <div class="form-group">
                <label>Address Line 1</label>
                <input type="text" name="address_line_1" class="form-control" placeholder="Enter line 1 of your address" <?php $this->htmlHelper->DisplayValueFor($model, 'address_line_1'); ?> />
                <?php $this->htmlHelper->DisplayErrorFor($model, 'address_line_1'); ?>
            </div>
            <div class="form-group">
                <label>Address Line 2</label>
                <input type="text" name="address_line_2" class="form-control" placeholder="Enter line 2 of your address" <?php $this->htmlHelper->DisplayValueFor($model, 'address_line_2'); ?> />
                <?php $this->htmlHelper->DisplayErrorFor($model, 'address_line_2'); ?>
            </div>
            <div class="form-group">
                <label>City</label>
                <input type="text" name="city" class="form-control" placeholder="Enter the city you live in" <?php $this->htmlHelper->DisplayValueFor($model, 'city'); ?> />
                <?php $this->htmlHelper->DisplayErrorFor($model, 'city'); ?>
            </div>
            <div class="form-group">
                <label>Postcode</label>
                <input type="text" name="postcode" class="form-control" placeholder="Enter your postcode" <?php $this->htmlHelper->DisplayValueFor($model, 'postcode'); ?> />
                <?php $this->htmlHelper->DisplayErrorFor($model, 'postcode'); ?>
            </div>
        </div>

        <div  class="wizard-page" data-target="3">
            <div class="form-group">
                <label>Are you an Investor or Landlord?</label>
                <p>
                    What type of account do you want to create?
                    Investors and Landlords can invest and
                    list property but your account will be geared towards
                    your chosen area. This can be changed in the account section
                    at a later date.
                </p>
                <select class="form-control" name="account_type">
                    <option value="investor">I want to invest in property</option>
                    <option value="landlord">I want to list my property</option>
                </select>
            </div>
        </div>

        <input type="hidden" name="id" value="<?= $model->id; ?>" />
        <input type="hidden" name="email" value="<?= $model->email; ?>" />

        <div class="clearfix"></div>
        <p class="terms" style="font-size: 10px;">By clicking 'Accept & Finish' you agree to follow the <a href="/home">terms & conditions</a> of EquityHolder.com</p>
        <div class="clearfix"></div>

        <a href="#" class="btn btn-primary btn-lg wizard-nav wizard-nav-back" data-direction="back">Back</a>
        <a href="#" class="btn btn-primary btn-lg wizard-nav wizard-nav-next pull-right" data-direction="next">Next</a>
        <button type="submit" class="btn btn-primary btn-lg wizard-nav-submit pull-right">Accept & Finish</button>

        <div class="clearfix"></div>
    </form>
</div>

<script>
    $(function(){

        var currentPage = 1;
        var targetLength = 3;

        $('.wizard-nav').click(function(e){

            e.preventDefault();

            var direction = $(this).attr('data-direction');
            if(direction == 'back')
            {
                if(currentPage > 1){
                    currentPage--;
                }
            }
            else
            {
                currentPage++;
            }

            if(currentPage < targetLength + 1)
            {
                openPage();
            }
        });

        function openPage()
        {
            $('.wizard-page').hide();
            $('.wizard-page[data-target='+ currentPage +']').show();
            $('.current-wizard').html(currentPage);

            //First page
            if(currentPage == 1)
            {
                $('.wizard-nav-back').hide();
                $('.wizard-nav-submit').hide();
                $('.terms').hide();
            }

            //Any page
            if(currentPage > 1 && currentPage < targetLength + 1)
            {
                $('.wizard-nav-back').show();
                $('.wizard-nav-next').show();
                $('.wizard-nav-submit').hide();
                $('.terms').hide();
            }

            //Lat page
            if(currentPage == targetLength)
            {
                $('.wizard-nav-next').hide()
                $('.wizard-nav-submit').show();
                $('.terms').show();
            }

        }

        var wizardPage = '<?php if(isset($model->modelErrors) && isset($model->modelErrors[0])){ $this->htmlHelper->GetAssociativeArrayByIndex($model->modelErrors, 0); }; ?>';

        if(wizardPage != '')
        {
            currentPage = wizardPage;
            openPage();
        }
        else
        {
            openPage();
        }

    });
</script>
