<div class="container" style="text-align: center; padding-top: 60px;">
    <h2>
        <span class="glyphicon glyphicon-ok"></span>
        Your account has been created
    </h2>
    <p>Please check your email and follow the instructions to complete your registration.</p>
</div>