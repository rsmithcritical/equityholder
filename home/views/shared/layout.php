<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require('header.php'); ?>
    </head>
    <body>
        <?php require('nav.php'); ?>
        <?php
            if(isset($model) && isset($model->message))
            {
                echo $model->message;
            }
        ?>
        <div style="min-height: 400px;">
            <?php require($location);  ?>
        </div>
        <?php require('footer.php'); ?>
    </body>
</html>