<footer>
    <div class="footer-container">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h4 class="footer-nav-title">For Landlords</h4>
                    <ul class="footer-nav">
                        <li><a href="#">How do I list property?</a></li>
                        <li><a href="#">How do I open an account?</a></li>
                        <li><a href="#">How do I get paid?</a></li>
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4 class="footer-nav-title">For Investors</h4>
                    <ul class="footer-nav">
                        <li><a href="#">How do I invest in property?</a></li>
                        <li><a href="#">How do I open an account?</a></li>
                        <li><a href="#">How do I get my profits?</a></li>
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h4 class="footer-nav-title">To DO: Social Media Widgets to go here...</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <h4 class="footer-nav-title">About EquityHolder</h4>
                    <ul class="footer-nav">
                        <li><a href="#">What is EquityHolder?</a></li>
                        <li><a href="#">How can EquityHolder help me?</a></li>
                        <li><a href="#">Why choose EquityHolder?</a></li>
                        <li><a href="#">About EquityHolder.com</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4 class="footer-nav-title">Support & Docs</h4>
                    <ul class="footer-nav">
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-md-6">

                </div>
            </div>
            <p class="footer-copy">
                <span>&copy; EquityHolder.com 2015</span>
                        <span class="pull-right footer-sub-nav">
                            <a href="#">T&Cs</a> |
                            <a href="#">Privacy Policy</a> |
                            <a href="#">Cookies</a>
                        </span>
            </p>
        </div>
    </div>
</footer>