<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/home">
                <img src="/home/public/img/logo.png" alt="" />
            </a>
        </div>
        <div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav navbar-right main-account-nav">
                <li><a href="/home/account/signup" style="padding-right: 10px;">Sign up</a></li>
                <li class="divider-vertical-small"></li>
                <li><a href="/home/account/login">Log in</a></li>
            </ul>
            <div class="clearfix"></div>
            <ul class="nav navbar-nav navbar-right main-site-nav">
                <li><a href="#">HOW IT WORKS</a></li>
                <li class="divider-vertical"></li>
                <li><a href="#">INVEST</a></li>
                <li class="divider-vertical"></li>
                <li><a href="#">LIST PROPERTY</a></li>
            </ul>
        </div>
    </div>
</nav>
