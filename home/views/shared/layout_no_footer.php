<!DOCTYPE html>
<html lang="en">
<head>
    <?php require('header.php'); ?>
</head>
<body>
<?php require('nav.php'); ?>
<?php
if (isset($model) && isset($model->message)) {
    echo $model->message;
}
?>
<?php require($location); ?>
</body>
</html>