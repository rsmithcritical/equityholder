<div class="slider">
    <img src="/home/public/img/temp/home_2.jpg"/>
</div>

<!-- TRENDING INVESTMENTS -->
<section class="trending-investments-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">
                    <span class="glyphicon glyphicon-fire"></span>
                    Trending
                    <a href="#" class="btn btn-default btn-lg pull-right">view more</a>
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="trending-investments">
                    <h4>London</h4>
                    <img src="/home/public/img/temp/investments/1.jpg" alt=""/>

                    <p class="funded">£252,000 <span>GBP</span></p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 23%;"></div>
                    </div>
                    <p>
                        <span class="pull-left">23%</span>
                        <span class="pull-right">30 days left</span>
                    </p>

                    <div class="clearfix"></div>
                    <a href="#" class="btn btn-default">view details</a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="trending-investments">
                    <h4>Stratford</h4>
                    <img src="/home/public/img/temp/investments/2.jpg" alt=""/>

                    <p class="funded">£252,000 <span>GBP</span></p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 42%;"></div>
                    </div>
                    <p>
                        <span class="pull-left">42%</span>
                        <span class="pull-right">21 days left</span>
                    </p>

                    <div class="clearfix"></div>
                    <a href="#" class="btn btn-default">view details</a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="trending-investments">
                    <h4>Cardiff</h4>
                    <img src="/home/public/img/temp/investments/3.jpg" alt=""/>

                    <p class="funded">£252,000 <span>GBP</span></p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 69%;"></div>
                    </div>
                    <p>
                        <span class="pull-left">69%</span>
                        <span class="pull-right">10 days left</span>
                    </p>

                    <div class="clearfix"></div>
                    <a href="#" class="btn btn-default">view details</a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="trending-investments">
                    <h4>Hull</h4>
                    <img src="/home/public/img/temp/investments/4.jpg" alt=""/>

                    <p class="funded">£252,000 <span>GBP</span></p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 96%;"></div>
                    </div>
                    <p>
                        <span class="pull-left">96%</span>
                        <span class="pull-right">5 days left</span>
                    </p>

                    <div class="clearfix"></div>
                    <a href="#" class="btn btn-default">view details</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- RECENTLY FUNDED -->
<section class="recently-funded-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h2 class="section-title">
                    <span class="glyphicon glyphicon-check"></span>
                    Recently funded
                    <a href="#" class="btn btn-default btn-lg pull-right">view more</a>
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="recently-funded">
                    <h4>London</h4>
                    <img src="/home/public/img/temp/investments/5.jpg" alt=""/>

                    <p class="funded">£927,000 <span>GBP</span></p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 100%;"></div>
                    </div>
                    <p>
                        <span class="pull-left">100%</span>
                        <span class="pull-right">Fully funded!</span>
                    </p>

                    <div class="clearfix"></div>
                    <a href="#" class="btn btn-default">view details</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="recently-funded">
                    <h4>Edinburgh</h4>
                    <img src="/home/public/img/temp/investments/6.jpg" alt=""/>

                    <p class="funded">£927,000 <span>GBP</span></p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 100%;"></div>
                    </div>
                    <p>
                        <span class="pull-left">100%</span>
                        <span class="pull-right">Fully funded!</span>
                    </p>

                    <div class="clearfix"></div>
                    <a href="#" class="btn btn-default">view details</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="recently-funded">
                    <h4>Essex</h4>
                    <img src="/home/public/img/temp/investments/7.jpg" alt=""/>

                    <p class="funded">£927,000 <span>GBP</span></p>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 100%;"></div>
                    </div>
                    <p>
                        <span class="pull-left">100%</span>
                        <span class="pull-right">Fully funded!</span>
                    </p>

                    <div class="clearfix"></div>
                    <a href="#" class="btn btn-default">view details</a>
                </div>
            </div>
        </div>
    </div>
</section>
