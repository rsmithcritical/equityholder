<?php

//Require base classes
require_once '../classes/loader.php';
require_once '../classes/basecontroller.php';
require_once '../classes/basemodel.php';
require_once '../classes/messagetype.php';
require_once '../classes/email.php';
require_once '../classes/modelerror.php';

//Require helpers
require_once 'helpers/htmlhelper.php';

//Require models
require_once 'models/homemodel.php';
require_once 'models/errormodel.php';
require_once 'models/accountmodel.php';

//Require controllers
require_once 'controllers/error.php';
require_once 'controllers/home.php';
require_once 'controllers/account.php';

//create controllers and execute the action
$loader = new Loader($_GET);

$controller = $loader->CreateController();
$controller->ExecuteAction();